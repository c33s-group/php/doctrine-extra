<?php

declare(strict_types=1);

// This is global bootstrap for autoloading

//use Codeception\Util\Autoload;

//Autoload::addNamespace('Tests\Traits', __DIR__.'/_support/Traits');
//Autoload::addNamespace('Tests\Functional', __DIR__.'/functional');
//Autoload::addNamespace('Tests\Acceptance', __DIR__.'/acceptance');
//Autoload::addNamespace('Tests\Unit', __DIR__.'/unit');

// https://github.com/Codeception/Codeception/issues/5411
// force test environment
//$_ENV["APP_ENV"]="test";
// load the symfony bootstrap file to ensure correct dotenv handling.
//require dirname(__DIR__).'/config/bootstrap.php';
