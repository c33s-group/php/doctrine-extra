<?php

declare(strict_types=1);

namespace Tests\Classes\JsonValueObjectTest\Value;

final class AddressLine
{
    private $content;

    public static function fromString(string $content): self
    {
        return new self($content);
    }

    private function __construct(string $content)
    {
        $this->content = $content;
    }

    public function content(): string
    {
        return $this->content;
    }
}
