<?php

declare(strict_types=1);

namespace Tests\Classes\JsonValueObjectTest\Value;

final class Zip
{
    private $code;

    public static function fromString(string $content): self
    {
        return new self($content);
    }

    public static function fromInt(int $content): self
    {
        return new self((string) $content);
    }

    private function __construct(string $content)
    {
        $this->code = $content;
    }

    public function code(): string
    {
        return $this->code;
    }
}
