<?php

declare(strict_types=1);

namespace Tests\Classes\NanoIdGeneratorTest;

use C33s\Doctrine\ORM\Id\RespectfulNanoIdGenerator;
use C33s\NanoId\NanoIdFactory;
use Doctrine\ORM\Mapping as ORM;
use Tests\Unit\C33s\Doctrine\ORM\Id\RespectfulNanoIdGeneratorTest;

/**
 * @ORM\Entity
 */
class NanoIdTestEntityWithLengthAndModeAndAlphabet
{
    /**
     * @ORM\Id
     * @ORM\Column(
     *     type="nanoid",
     *     unique=true,
     *     length=RespectfulNanoIdGeneratorTest::LONGER_TEST_LENGTH,
     *     options={
     *         "mode": NanoIdFactory::MODE_DYNAMIC,
     *         "alphabet": RespectfulNanoIdGeneratorTest::MINI_ALPHABET,
     *     }
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=RespectfulNanoIdGenerator::class)
     */
    public $id;

    /**
     * @ORM\Column(type="string", unique=true, length=60)
     */
    public $name;

    /**
     * Temp constructor.
     *
     * @param $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }
}
