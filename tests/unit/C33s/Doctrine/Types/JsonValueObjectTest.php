<?php

declare(strict_types=1);

namespace Tests\Unit\C33s\Doctrine\Types;

use C33s\Doctrine\Interfaces\ArrayInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use InvalidArgumentException;
use Tests\Classes\JsonValueObjectTest\AddressType;
use Tests\Classes\JsonValueObjectTest\AddressTypeWithWrongClass;
use Tests\Classes\JsonValueObjectTest\AddressValueObject;
use Tests\Traits\MakeDoctrineTypeObjectTrait;
use Tests\UnitTester;

/**
 * @group value
 * @group value-json
 */
class JsonValueObjectTest extends \Codeception\Test\Unit
{
    use MakeDoctrineTypeObjectTrait;

    /**
     * @var UnitTester
     */
    protected $tester;

    protected function getValidAddressArray()
    {
        return [
            'line1' => 'Example Street 1',
            'line2' => 'Office 42',
            'city' => 'Vienna',
            'zip' => '1010',
            'province' => 'Vienna',
            'country_code' => 'AT',
        ];
    }

    protected function getValidAddress()
    {
        return AddressValueObject::fromArray($this->getValidAddressArray());
    }

    protected function getValidAddressJson()
    {
//        return json_encode($this->getValidAddressArray(), JSON_THROW_ON_ERROR);
        return json_encode($this->getValidAddressArray());
    }

    /**
     * @test
     */
    public function JsonConvertToPHPValue()
    {
        $expected = $this->getValidAddress();

        $addressType = $this->getDoctrineTypeObjectFromClassString(AddressType::class);
        $platform = $this->make(AbstractPlatform::class);

        $actual = $addressType->convertToPHPValue($this->getValidAddressJson(), $platform);
        self::assertInstanceOf(AddressValueObject::class, $actual);
        self::assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function ValueObjectConvertToDatabaseValue()
    {
        $jsonValueObject = $this->getValidAddress();

        $AddressType = $this->getDoctrineTypeObjectFromClassString(AddressType::class);
        $platform = $this->make(AbstractPlatform::class);

        $actual = $AddressType->convertToDatabaseValue($jsonValueObject, $platform);
        $expected = $this->getValidAddressJson();

        self::assertJson($actual);
        self::assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function NullConvertToPHPValue()
    {
        $AddressType = $this->getDoctrineTypeObjectFromClassString(AddressType::class);
        $platform = $this->make(AbstractPlatform::class);

        $actual = $AddressType->convertToPHPValue(null, $platform);
        self::assertNull($actual);
    }

    /**
     * @test
     * @dataProvider WrongValueConvertToPHPValueLeadsToExceptionDataProvider
     *
     * @param mixed $value
     */
    public function WrongValueConvertToPHPValueLeadsToException($value)
    {
        $AddressType = $this->getDoctrineTypeObjectFromClassString(AddressType::class);
        $platform = $this->make(AbstractPlatform::class);

        $this->expectException(InvalidArgumentException::class);
        $AddressType->convertToPHPValue($value, $platform);
    }

    public function WrongValueConvertToPHPValueLeadsToExceptionDataProvider()
    {
        return [
            [1],
            [''],
            ['a string'],
            [['1', '2', '3']],
        ];
    }

    /**
     * @test
     */
    public function TypeClassNotImplementingArrayInterfaceLeadsToException()
    {
        $addressType = $this->getDoctrineTypeObjectFromClassString(AddressTypeWithWrongClass::class);
        $platform = $this->make(AbstractPlatform::class);
        $valueObjectClass = $this->tester->getProtectedProperty($addressType, 'valueObjectClass');
        $this->expectException(InvalidArgumentException::class);
        $message =
            'Doctrine Type "'.get_class($addressType)."'s\" value object class '$valueObjectClass'".
            ' has to implement '.ArrayInterface::class
        ;
        $this->expectExceptionMessage($message);

        $addressType->convertToPHPValue($this->getValidAddressJson(), $platform);
    }

    /**
     * @test
     */
    public function WrongValueObjectLeadsToException()
    {
        $wrongClass = new class() {
        };

        $AddressType = $this->getDoctrineTypeObjectFromClassString(AddressType::class);
        $platform = $this->make(AbstractPlatform::class);

        $this->expectException(ConversionException::class);
        $this->expectExceptionMessage('Could not convert database value "unknown (anon class)" to Doctrine Type test_address');
        $AddressType->convertToDatabaseValue($wrongClass, $platform);
    }
}
