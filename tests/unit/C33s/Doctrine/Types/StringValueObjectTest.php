<?php

declare(strict_types=1);

namespace Tests\Unit\C33s\Doctrine\Types;

use C33s\Doctrine\Interfaces\StringInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use InvalidArgumentException;
use Tests\Classes\StringValueObjectTest\EmailType;
use Tests\Classes\StringValueObjectTest\EmailTypeWithWrongClass;
use Tests\Classes\StringValueObjectTest\EmailValueObject;
use Tests\Traits\MakeDoctrineTypeObjectTrait;
use Tests\UnitTester;

/**
 * @group value
 * @group value-string
 */
class StringValueObjectTest extends \Codeception\Test\Unit
{
    use MakeDoctrineTypeObjectTrait;

    /**
     * @var UnitTester
     */
    protected $tester;

    protected function getValidEmail()
    {
        return EmailValueObject::fromString('name@email.com');
    }

    /**
     * @test
     */
    public function StringConvertToPHPValue()
    {
        $expected = EmailValueObject::fromString('name@email.com');

        $emailType = $this->getDoctrineTypeObjectFromClassString(EmailType::class);
        $platform = $this->make(AbstractPlatform::class);

        $actual = $emailType->convertToPHPValue('name@email.com', $platform);
        self::assertInstanceOf(EmailValueObject::class, $actual);
        self::assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function ValueObjectConvertToDatabaseValue()
    {
        $emailValueObject = $this->getValidEmail();

        $emailType = $this->getDoctrineTypeObjectFromClassString(EmailType::class);
        $platform = $this->make(AbstractPlatform::class);

        $actual = $emailType->convertToDatabaseValue($emailValueObject, $platform);

        self::assertIsString($actual);
        self::assertEquals('name@email.com', $actual);
    }

    /**
     * @test
     */
    public function NullConvertToPHPValue()
    {
        $emailType = $this->getDoctrineTypeObjectFromClassString(EmailType::class);
        $platform = $this->make(AbstractPlatform::class);

        $actual = $emailType->convertToPHPValue(null, $platform);
        self::assertNull($actual);
    }

    /**
     * @test
     * @dataProvider WrongValueConvertToPHPValueLeadsToExceptionDataProvider
     *
     * @param mixed $value
     */
    public function WrongValueConvertToPHPValueLeadsToException($value)
    {
        $emailType = $this->getDoctrineTypeObjectFromClassString(EmailType::class);
        $platform = $this->make(AbstractPlatform::class);

        $this->expectException(InvalidArgumentException::class);
        $emailType->convertToPHPValue($value, $platform);
    }

    public function WrongValueConvertToPHPValueLeadsToExceptionDataProvider()
    {
        return [
            [1],
            [['1', '2', '3']],
        ];
    }

    /**
     * @test
     */
    public function TypeClassNotImplementingArrayInterfaceLeadsToException()
    {
        $emailType = $this->getDoctrineTypeObjectFromClassString(EmailTypeWithWrongClass::class);
        $platform = $this->make(AbstractPlatform::class);
        $valueObjectClass = $this->tester->getProtectedProperty($emailType, 'valueObjectClass');
        $this->expectException(InvalidArgumentException::class);
        $message =
            'Doctrine Type "'.get_class($emailType)."'s\" value object class '$valueObjectClass'".
            ' has to implement '.StringInterface::class
        ;
        $this->expectExceptionMessage($message);

        $emailType->convertToPHPValue('foo@bar.com', $platform);
    }

    /**
     * @test
     */
    public function InvalidValueObjectLeadsToException()
    {
        $InvalidValueObject = new class() {
        };

        $emailType = $this->getDoctrineTypeObjectFromClassString(EmailType::class);
        $platform = $this->make(AbstractPlatform::class);

        $this->expectException(ConversionException::class);
        $message = 'Could not convert database value "unknown (anon class)" to Doctrine Type test_email';
        $this->expectExceptionMessage($message);

        $emailType->convertToDatabaseValue($InvalidValueObject, $platform);
    }
}
