<?php

declare(strict_types=1);

namespace Tests\Unit\C33s\Doctrine\ORM\Id;

use C33s\Doctrine\ORM\Id\RespectfulUuidGenerator;
use Codeception\Test\Unit;
use Doctrine\ORM\Mapping\MappingException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Tests\Classes\RespectfulUuidGeneratorTest\DualIdTestClass;
use Tests\Classes\RespectfulUuidGeneratorTest\IdTestEntity;
use Tests\Unit\Traits\DatabaseIdTrait;

/**
 * @group generator
 * @group uuid
 */
class RespectfulUuidGeneratorTest extends Unit
{
    use DatabaseIdTrait;

    /**
     * @test
     */
    public function generateWithPredefinedEntityUuid(): void
    {
        $em = $this->getEntityManager();
        $uuidGenerator = new RespectfulUuidGenerator();
        $uuid = Uuid::uuid4();
        $user = new IdTestEntity($uuid);
        $generatedUuid = $uuidGenerator->generate($em, $user);
        $this->assertNotNull($generatedUuid);
        $this->assertInstanceOf(UuidInterface::class, $generatedUuid);
        $this->assertTrue($generatedUuid->equals($uuid));
    }

    /**
     * @test
     */
    public function generateWithNoEntityUuid(): void
    {
        $em = $this->getEntityManager();
        $uuidGenerator = new RespectfulUuidGenerator();
        $user = new IdTestEntity();
        $generatedUuid = $uuidGenerator->generate($em, $user);
        $this->assertNotNull($generatedUuid);
        $this->assertInstanceOf(UuidInterface::class, $generatedUuid);
    }

    /**
     * @test
     */
    public function generateWithNoEntityUuidDualId(): void
    {
        $this->expectException(MappingException::class);
        $this->expectExceptionMessage('composite identifier');
        $em = $this->getEntityManager();
        $uuidGenerator = new RespectfulUuidGenerator();
        $user = new DualIdTestClass();
        $generatedUuid = $uuidGenerator->generate($em, $user);
        $this->assertNotNull($generatedUuid);
        $this->assertInstanceOf(UuidInterface::class, $generatedUuid);
    }

    /**
     * @test
     */
    public function generateWithPredefinedEntityUuidDualId(): void
    {
        $this->expectException(MappingException::class);
        $this->expectExceptionMessage('composite identifier');
        $em = $this->getEntityManager();
        $uuidGenerator = new RespectfulUuidGenerator();
        $uuid = Uuid::uuid4();
        $user = new DualIdTestClass($uuid);
        $generatedUuid = $uuidGenerator->generate($em, $user);
        $this->assertNotNull($generatedUuid);
        $this->assertInstanceOf(UuidInterface::class, $generatedUuid);
        $this->assertTrue($generatedUuid->equals($uuid));
    }

    /**
     * @test
     */
    public function generateUuidForAnonymousClass(): void
    {
        $em = $this->getEntityManager();
        $uuidGenerator = new RespectfulUuidGenerator();
        $expectedUuid = Uuid::uuid4();
        $user = new class($expectedUuid) {
            public $id;

            public function __construct($id = null)
            {
                $this->id = $id;
            }
        };
        $generatedUuid = $uuidGenerator->generate($em, $user);
        $this->assertNotNull($generatedUuid);
        $this->assertInstanceOf(UuidInterface::class, $generatedUuid);
        $this->assertTrue($generatedUuid->equals($expectedUuid));
    }

    /**
     * @test
     */
    public function generateUuidForAnonymousClassWithoutConstructor(): void
    {
        $em = $this->getEntityManager();
        $uuidGenerator = new RespectfulUuidGenerator();
        $user = new class() {
            public $id;
        };
        $generatedUuid = $uuidGenerator->generate($em, $user);
        $this->assertNotNull($generatedUuid);
        $this->assertInstanceOf(UuidInterface::class, $generatedUuid);
    }
}
