<?php

declare(strict_types=1);

namespace Tests\Unit\Traits;

use C33s\Doctrine\Types\NanoIdType;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use InvalidArgumentException;

trait DatabaseIdTrait
{
    private $typeMap = [
      'nanoid' => NanoIdType::class,
    ];

    /**
     * Define additional types you want to use in the $additionalTypes parameter. The typename with its class has to be
     * defined in the EntityManagerTrait::$typeMap before.
     */
    protected function getEntityManager(array $additionalTypes = []): EntityManagerInterface
    {
        $baseDir = codecept_data_dir();
        $entityDir = $baseDir.'/Classes';
        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration([$entityDir], $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

        $conn = [
            'driver' => 'pdo_sqlite',
            'path' => codecept_output_dir().'/test.sqlite',
        ];

        $em = EntityManager::create($conn, $config);

        foreach ($additionalTypes as $typeName) {
            $this->addType($em, $typeName);
        }

        return $em;
    }

    private function addType(EntityManagerInterface $em, string $typeName): void
    {
        if (!array_key_exists($typeName, $this->typeMap)) {
            $map = implode(',', array_keys($this->typeMap));
            throw new InvalidArgumentException("Given type '$typeName' not available in type map ($map)");
        }
        Type::addType($typeName, $this->typeMap[$typeName]);
        $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('nanoid', 'nanoid');
    }
}
