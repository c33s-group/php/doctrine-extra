<?php

declare(strict_types=1);

namespace Tests\Traits;

use ReflectionObject;

/**
 * Unit Test Usage:.
 *
 * <code>
 * `Helper\Unit`:
 *
 * class Unit extends \Codeception\Module
 * {
 *    use ProtectedMethodAccessTrait;
 * }
 * </code>
 *
 * In the test class add:
 *
 * <code>
 *   use Tests\UnitTester;
 *   **
 *    * @var UnitTester
 *    *
 *   protected $tester;
 * </code>
 *
 * and use it with
 *
 * <code>
 * $this->tester->getProtectedProperty($class, 'propertyName');
 * </code>
 *
 * Accessible with $this->test
 *
 * Functional Test Usage:
 *
 * <code>
 * `Helper\Functional`:
 *
 * class Functional extends \Codeception\Module
 * {
 *     use ProtectedMethodAccessTrait;
 * }
 * </code>
 *
 * use it in the test class:
 * <code>
 *     $I->getProtectedProperty($class, 'propertyName');
 * </code>
 */
trait ProtectedMethodAccessTrait
{
    /**
     * Returns the previous protected method from a class as public method, which now can be invoked.
     *
     * ```php
     * $service = new ExampleService();
     * $method = self::getProtectedMethod('exampleMethodName', ExampleService::class);
     * $params = []
     * $result = $method->invokeArgs($service, $params)
     * ```
     *
     * @param $methodName
     * @param $className
     *
     * @return \ReflectionMethod
     */
    protected static function getProtectedMethod($methodName, $className)
    {
        $class = new \ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * Runs a protected method from a class and returns its result.
     *
     * ```php
     * $params = []
     * $result = self::callProtectedMethod('exampleMethodName', ExampleService::class, $params);
     * ```
     *
     * @param $methodName
     * @param $className
     * @param $methodParameters
     *
     * @return mixed
     */
    public static function callProtectedMethod($methodName, $className, $methodParameters = [])
    {
        $method = self::getProtectedMethod($methodName, $className);
        $object = new $className($methodParameters);

        return $method->invokeArgs($object, $methodParameters);
    }

    /**
     * Returns the value of a protected Property.
     *
     * @param $propertyName
     * @param $class
     *
     * @throws \ReflectionException
     *
     * @return mixed
     */
    public static function getProtectedProperty($class, $propertyName)
    {
        $property = (new ReflectionObject($class))->getProperty($propertyName);
        $property->setAccessible(true);

        return  $property->getValue($class);
    }
}
