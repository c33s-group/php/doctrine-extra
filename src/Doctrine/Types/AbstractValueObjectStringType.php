<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types;

use C33s\Doctrine\Types\Interfaces\StringTypeConversionInterface;
use C33s\Doctrine\Types\Traits\StringInterfaceConversionTrait;
use Doctrine\DBAL\Types\StringType;

abstract class AbstractValueObjectStringType extends StringType implements StringTypeConversionInterface
{
    use StringInterfaceConversionTrait;
}
