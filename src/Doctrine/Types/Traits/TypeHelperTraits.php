<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types\Traits;

use Exception;
use function json_decode;
use ReflectionClass;

trait TypeHelperTraits
{
    /**
     * @param mixed $value
     *
     * @return bool
     */
    private function isJson($value)
    {
        if (!is_string($value)) {
            return false;
        }

        json_decode($value);

        if (json_last_error()) {
            return false;
        }

        return true;
    }

    /**
     * Builds a string error message from different kind of types.
     *
     * @param mixed $value
     *
     * @return string
     */
    private function buildErrorMessage($value)
    {
        if (is_object($value) && $this->isAnonymous($value)) {
            return 'unknown (anon class)';
        }

        if (is_object($value) && $this->isSerializable($value)) {
            return serialize($value);
        }

        if (is_object($value)) {
            return 'unknown (unserializeable class)';
        }

        return (string) $value;
    }

    /**
     * @param mixed $instance
     *
     * @return bool
     */
    private function isAnonymous($instance)
    {
        if (!is_object($instance)) {
            return false;
        }

        return (new ReflectionClass($instance))->isAnonymous();
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isSerializable($value)
    {
        try {
            serialize($value);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
