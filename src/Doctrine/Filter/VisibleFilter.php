<?php

declare(strict_types=1);

namespace C33s\Doctrine\Filter;

use C33s\Doctrine\Filter\Interfaces\VisibleInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Informational Links.
 *
 * <info>
 * https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/reference/filters.html
 * In test mode Symfony can't find doctrine filters
 * https://github.com/symfony/symfony/issues/29772
 * https://github.com/Atlantic18/DoctrineExtensions/blob/v2.4.x/lib/Gedmo/SoftDeleteable/Filter/SoftDeleteableFilter.php.
 * </info>
 */
final class VisibleFilter extends SQLFilter
{
    /**
     * @param string $targetTableAlias
     *
     * @return string
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (!$targetEntity->reflClass->implementsInterface(VisibleInterface::class)) {
            return '';
        }

        return $targetTableAlias.'.visible = true';
    }
}
