<?php

declare(strict_types=1);

namespace C33s\Doctrine\DataFixtures\Types;

use C33s\ParameterObjects\KernelEnvironment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Fidry\AliceDataFixtures\LoaderInterface;

abstract class EnvironmentSpecificFixture extends Fixture implements OrderedFixtureInterface
{
    /**
     * @var string
     */
    protected $currentEnvironment;

    /**
     * @var LoaderInterface
     */
    protected $aliceLoader;

    public function __construct(KernelEnvironment $currentEnvironment, LoaderInterface $aliceLoader)
    {
        $this->currentEnvironment = $currentEnvironment->value();
        $this->aliceLoader = $aliceLoader;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        if (['all'] !== $this->getNormalizedDataLoadEnvironments()
            && !in_array($this->currentEnvironment, $this->getNormalizedDataLoadEnvironments(), true)
        ) {
            return;
        }

        $this->doLoad($manager);
    }

    protected function getNormalizedDataLoadEnvironments(): array
    {
        return $this->getDataLoadEnvironments();
    }

    /**
     * Perform the database objects.
     *
     * @param ObjectManager $manager the object manager
     *
     * @return mixed
     */
    abstract protected function doLoad(ObjectManager $manager);

    /**
     * Get the environments the data fixtures are ran on.
     *
     * @return array the name of the environments
     */
    abstract protected function getDataLoadEnvironments(): array;
}
