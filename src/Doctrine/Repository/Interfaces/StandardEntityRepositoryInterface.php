<?php

declare(strict_types=1);

namespace C33s\Doctrine\Repository\Interfaces;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\Proxy as LegacyProxy;
use Doctrine\Persistence\Proxy;

/**
 * @template T
 */
interface StandardEntityRepositoryInterface
{
    /**
     * @param object|Collection|LegacyProxy|LegacyProxy[]|Proxy|Proxy[] $objects
     */
    public function save($objects, bool $doFlush = true): void;

    /**
     * @param object|Collection|LegacyProxy|LegacyProxy[]|Proxy|Proxy[] $objects
     */
    public function delete($objects, bool $doFlush = true): void;

    /**
     * @param mixed     $id
     * @param bool|null $lockMode
     * @param null      $lockVersion
     *
     * @return mixed
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * Finds a single entity by a set of criteria.
     *
     * @psalm-param array<string, mixed> $criteria
     * @psalm-param array<string, string>|null $orderBy
     *
     * @return object|null the entity instance or NULL if the entity can not be found
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return mixed
     */
    public function findAll();

    /**
     * Finds entities by a set of criteria.
     *
     * @param int|null $limit
     * @param int|null $offset
     *
     * @psalm-param array<string, mixed> $criteria
     * @psalm-param array<string, string>|null $orderBy
     *
     * @return object[] the objects
     *
     * @psalm-return list<T>
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}
