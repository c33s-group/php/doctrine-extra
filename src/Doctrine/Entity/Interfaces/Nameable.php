<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Interfaces;

interface Nameable
{
    public function getName(): ?string;

    /**
     * @return mixed
     */
    public function setName(?string $name);
}
