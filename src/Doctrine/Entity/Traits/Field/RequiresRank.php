<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait RequiresRank.
 *
 * Traits cannot define constants, so the constants have to be defined in the entity class where the trait is used (an
 * alternative would be to use static variables).
 *
 * required constant:
 * <code>
 *     const DEFAULT_RANK = 50;
 * </code>
 */
trait RequiresRank
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\Type("integer")
     * @Assert\Range(min=1, max=100)
     * @Assert\NotNull()
     */
    protected $rank = self::DEFAULT_RANK;

    public function getRank(): int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }
}
