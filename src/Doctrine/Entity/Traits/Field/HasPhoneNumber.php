<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasPhoneNumber
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=36, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max=36)
     */
    protected $phoneNumber;

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }
}
