<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasPreTitle
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max=15)
     */
    protected $preTitle;

    public function getPreTitle(): ?string
    {
        return $this->preTitle;
    }

    public function setPreTitle(?string $preTitle): self
    {
        $this->preTitle = $preTitle;

        return $this;
    }
}
