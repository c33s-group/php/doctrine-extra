<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait RequiresPhoneNumber
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=36, nullable=false)
     * @Assert\Type("string")
     * @Assert\Length(max=36)
     * @Assert\NotBlank()
     */
    protected $phoneNumber;

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }
}
