<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait RequiresPreTitle
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=15, nullable=false)
     * @Assert\Type("string")
     * @Assert\Length(max=15)
     * @Assert\NotBlank()
     */
    protected $preTitle;

    public function getPreTitle(): ?string
    {
        return $this->preTitle;
    }

    public function setPreTitle(string $preTitle): self
    {
        $this->preTitle = $preTitle;

        return $this;
    }
}
