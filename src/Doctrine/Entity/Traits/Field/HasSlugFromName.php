<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Gedmo\Mapping\Annotation as Gedmo;

trait HasSlugFromName
{
    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true, nullable=true)
     */
    protected $slug;

    public function getSlug(): string
    {
        return $this->slug;
    }
}
