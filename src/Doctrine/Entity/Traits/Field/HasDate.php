<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

trait HasDate
{
    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function setDate(?DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }
}
