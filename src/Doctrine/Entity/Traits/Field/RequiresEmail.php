<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait RequiresEmail
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
