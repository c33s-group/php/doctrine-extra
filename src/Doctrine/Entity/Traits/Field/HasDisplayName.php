<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasDisplayName
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="255")
     */
    protected $displayName;

    /**
     * @return string
     */
    public function getDisplayName(): ?string
    {
        if (null !== $this->displayName) {
            return $this->displayName;
        }

        return "{$this->getFirstName()} {$this->getLastName()}";
    }

    /**
     * @param string $displayName
     *
     * @return HasDisplayName
     */
    public function setDisplayName(?string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    abstract public function getFirstName();

    abstract public function getLastName();
}
