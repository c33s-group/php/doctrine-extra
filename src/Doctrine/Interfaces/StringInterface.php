<?php

declare(strict_types=1);

namespace C33s\Doctrine\Interfaces;

interface StringInterface
{
    /**
     * @return self
     */
    public static function fromString(string $string);

    public function toString(): string;
}
