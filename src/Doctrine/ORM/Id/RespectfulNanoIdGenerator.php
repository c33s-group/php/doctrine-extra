<?php

declare(strict_types=1);

namespace C33s\Doctrine\ORM\Id;

use C33s\NanoId\NanoId;
use Doctrine\ORM\EntityManager;

final class RespectfulNanoIdGenerator extends AbstractIdGenerator
{
    /**
     * {@inheritdoc}
     */
    public function generate(EntityManager $em, $entity): ?NanoId
    {
        return parent::generate($em, $entity) ?? $this->generateNew();
    }

    private function generateNew(): NanoId
    {
        $mode = $this->getOption('mode');
        $alphabet = $this->getOption('alphabet');
        $length = $this->getLength();

        return NanoId::nanoId($length, $mode, $alphabet);
    }
}
