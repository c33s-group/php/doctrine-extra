<?php

declare(strict_types=1);

namespace C33s\Doctrine\ORM\Id;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator as BaseAbstractIdGenerator;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\Proxy;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractIdGenerator extends BaseAbstractIdGenerator
{
    private const ID_FIELD_NAME = 'id';

    /**
     * @var ClassMetadata
     */
    private $classMetadata;

    /**
     * {@inheritdoc}
     */
    public function generate(EntityManager $em, $entity)
    {
        $id = $this->isEntity($em, $entity) ? $this->getIdentifierValueFromEntity($em, $entity) : null;

        return $id ?? $this->getIdValueWithReflection($entity);
    }

    protected function getLength(): ?int
    {
        return $this->classMetadata->fieldMappings[self::ID_FIELD_NAME]['length'] ?? null;
    }

    /**
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    protected function getOption(string $name, $default = null)
    {
        return $this->classMetadata->fieldMappings[self::ID_FIELD_NAME]['options'][$name] ?? $default;
    }

    protected function getOptions(): array
    {
        return $this->classMetadata->fieldMappings[self::ID_FIELD_NAME]['options'] ?? [];
    }

    /**
     * @param object $entity
     */
    private function initClassMetaData(EntityManager $em, $entity): void
    {
        $this->classMetadata = $em->getClassMetadata(get_class($entity));
    }

    /**
     * Based upon TomasVotruba https://github.com/ramsey/uuid-doctrine/issues/81#issuecomment-510790171.
     *
     * @param mixed $class
     *
     * @return mixed|null
     */
    private function getIdValueWithReflection($class)
    {
        $idReflectionProperty = $this->findClassIdProperty($class);

        if (null === $idReflectionProperty) {
            return null;
        }

        return $idReflectionProperty->getValue($class);
    }

    /**
     * @param object $object
     */
    private function findClassIdProperty($object): ?ReflectionProperty
    {
        $reflectionClass = new ReflectionClass($object);

        do {
            if ($reflectionClass->hasProperty('id')) {
                $idReflectionProperty = $reflectionClass->getProperty('id');
                $idReflectionProperty->setAccessible(true);

                return $idReflectionProperty;
            }
            // phpcs:disable Generic.CodeAnalysis.AssignmentInCondition.FoundInWhileCondition
        } while ($reflectionClass = $reflectionClass->getParentClass());
        // phpcs:enable
        return null;
    }

    /**
     * @param mixed $object
     *
     * @see https://stackoverflow.com/questions/20894705/how-to-check-if-a-class-is-a-doctrine-entity
     */

    /**
     * @param object $object
     */
    private function isEntity(EntityManager $em, $object): bool
    {
        if (!is_object($object)) {
            return false;
        }
        $class = ($object instanceof Proxy)
            ? get_parent_class($object)
            : get_class($object)
        ;
        if (false === $class) {
            return false;
        }
//        return ! $em->getMetadataFactory()->isTransient($class);
        try {
            $em->getRepository($class);
        } catch (MappingException $e) {
            if (false !== strpos($e->getMessage(), 'Unknown Entity namespace alias')) {
                return false;
            }

            if (false !== strpos($e->getMessage(), 'not a valid entity or mapped super class.')) {
                return false;
            }
            throw $e;
        } catch (ORMException $e) {
            if (false !== strpos($e->getMessage(), 'Unknown Entity namespace alia')) {
                return false;
            }
            throw $e;
        }

        return true;
    }

    /**
     * @param object $entity
     *
     * @return mixed|null
     */
    private function getIdentifierValueFromEntity(EntityManager $em, $entity)
    {
        $this->initClassMetaData($em, $entity);
        $classMetadata = $this->classMetadata;

        $idFields = $classMetadata->getIdentifierFieldNames();

        if (1 !== count($idFields)) {
            throw new \InvalidArgumentException('Respectful Id Generator works only with doctrine entities 
            having only one identifier');
        }
        $idField = $idFields[0];
        $value = $classMetadata->getFieldValue($entity, $idField);

        if (isset($classMetadata->associationMappings[$idField])) {
            // NOTE: Single Columns as associated identifiers only allowed - this constraint it is enforced.
            $value = $em->getUnitOfWork()->getSingleIdentifierValue($value);
        }

        return $value;
    }
}
