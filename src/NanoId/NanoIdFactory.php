<?php

declare(strict_types=1);

namespace C33s\NanoId;

use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\CoreInterface;
use InvalidArgumentException;

final class NanoIdFactory
{
    public const DEFAULT_LENGTH = 8;
    public const MODE_NORMAL = Client::MODE_NORMAL;
    public const MODE_DYNAMIC = Client::MODE_DYNAMIC;
    public const AVAILABLE_MODES = [
        self::MODE_NORMAL,
        self::MODE_DYNAMIC,
    ];

    private const DEFAULT_MODE = self::MODE_NORMAL;
    public const DEFAULT_ALPHABET = CoreInterface::SAFE_SYMBOLS;

    /**
     * @var int|null
     */
    private $mode;

    /**
     * @var string|null
     */
    private $alphabet;

    /**
     * @var Client
     */
    private $client;

    public function __construct(
        ?int $length = self::DEFAULT_LENGTH,
        ?int $mode = self::DEFAULT_MODE,
        ?string $alphabet = self::DEFAULT_ALPHABET
    ) {
        $this->validateMode($mode);
        $this->mode = $mode;
        $this->alphabet = $alphabet;
        $this->client = new Client($length);
    }

    public function generate(?int $length = null, ?int $mode = null, ?string $alphabet = null): NanoId
    {
        $this->validateMode($mode);
        $mode = $mode ?? $this->mode;
        if (self::MODE_NORMAL === $mode && null !== $alphabet) {
            throw new InvalidArgumentException('cannot use normal mode with custom alphabet');
        }
        $alphabet = $alphabet ?? $this->alphabet;

        if (null !== $alphabet) {
            $id = $this->client->formattedId($alphabet, $length);
        } else {
            $id = $this->client->generateId($length, $mode);
        }

        return NanoId::fromString($id);
    }

    private function validateMode(?int $mode): void
    {
        if (null === $mode) {
            return;
        }

        if (!in_array($mode, self::AVAILABLE_MODES, true)) {
            $map = implode(', ', self::AVAILABLE_MODES);
            throw new InvalidArgumentException("Given type '$mode' not available in type map ($map)");
        }
    }
}
