<?php

declare(strict_types=1);

namespace C33s\NanoId;

use C33s\Doctrine\Interfaces\StringInterface;
use JsonSerializable;
use Serializable;

final class NanoId implements StringInterface, JsonSerializable, Serializable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var NanoIdFactory
     */
    private static $factory;

    public static function fromString(string $string): self
    {
        return new self($string);
    }

    public static function nanoId(?int $length = null, ?int $mode = null, ?string $alphabet = null): self
    {
        return self::getFactory()->generate($length, $mode, $alphabet);
    }

    private static function getFactory(): NanoIdFactory
    {
        if (null === self::$factory) {
            self::$factory = new NanoIdFactory();
        }

        return self::$factory;
    }

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->id;
    }

    /**
     * Converts the NanoId to a string for JSON serialization.
     */
    public function jsonSerialize(): string
    {
        return $this->toString();
    }

    /**
     * Converts the NanoId to a string for PHP serialization.
     */
    public function serialize(): string
    {
        return $this->toString();
    }

    /**
     * Re-constructs the object from its serialized form.
     *
     * @param mixed $data
     */
    public function unserialize($data): void
    {
        $this->id = $data;
    }
}
